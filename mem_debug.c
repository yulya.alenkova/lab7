#include "mem.h"

void memalloc_debug_struct_info(FILE* f, struct mem const* const address) {
	

	fprintf(f, "start: %p\nsize: %lu\nis_free: %d\n",
			(void*) address ,
			address->capacity,
			address->is_free);

}

void memalloc_debug_heap(FILE* f, struct mem const* ptr) {
	for (; ptr; ptr = ptr->next )
		memalloc_debug_struct_info(f, ptr);
}
